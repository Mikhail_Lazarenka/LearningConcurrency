package executer;

import java.util.concurrent.*;

public class App {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                return 123;
            }
            catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };

        ExecutorService executor= Executors.newFixedThreadPool(1);
        Future<Integer> future= executor.submit(task);

        System.out.println("Future is done? "+future.isDone());
        Integer result= future.get();
        System.out.println(result);

    }
}
